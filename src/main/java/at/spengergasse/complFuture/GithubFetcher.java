package at.spengergasse.complFuture;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;

public class GithubFetcher {

    private static HttpClient client = HttpClient.newHttpClient();

    public static void main(String... args) {
        ArrayList<String> users = new ArrayList<>();
        users.add("gcla");
        users.add("bilibili");
        users.add("cool-RR");
        users.add("996icu");
        users.add("3b1b");
        users.add("MSWorkers");
        lookupReposOfUserSerial(users);
        lookupReposOfUserSerialAsync(users);
        lookupReposOfUserParallel(users);
        lookupReposOfUserParallelAsync(users);
    }

    private static HttpRequest search(String username) {
        return HttpRequest.newBuilder()
                .uri(URI.create("https://api.github.com/users/" + username + "/repos"))
                .build();
    }

    private static void lookupReposOfUserParallel(ArrayList<String> users) {
        long timeStart = System.currentTimeMillis();
        users.stream()
                .parallel()
                .forEach(r -> client.sendAsync(search(r), HttpResponse.BodyHandlers.ofString())
                        .thenApply(HttpResponse::body)
                        .join());

        System.out.println("Parallel took " + String.valueOf(System.currentTimeMillis() - timeStart) + " ms.");
    }

    private static void lookupReposOfUserParallelAsync(ArrayList<String> users) {
        long timeStart = System.currentTimeMillis();
        users.stream()
                .parallel()
                .forEach(r -> client.sendAsync(search(r), HttpResponse.BodyHandlers.ofString())
                        .thenApplyAsync(HttpResponse::body)
                        .join());

        System.out.println("Parallel Async took " + String.valueOf(System.currentTimeMillis() - timeStart) + " ms.");
    }

    private static void lookupReposOfUserSerial(ArrayList<String> users) {
        long timeStart = System.currentTimeMillis();
        users.stream()
                .sequential()
                .forEach(r -> client.sendAsync(search(r), HttpResponse.BodyHandlers.ofString())
                        .thenApply(HttpResponse::body)
                        .join());

        System.out.println("Serial took " + String.valueOf(System.currentTimeMillis() - timeStart) + " ms.");
    }

    private static void lookupReposOfUserSerialAsync(ArrayList<String> users) {
        long timeStart = System.currentTimeMillis();
        users.stream()
                .sequential()
                .forEach(r -> client.sendAsync(search(r), HttpResponse.BodyHandlers.ofString())
                        .thenApplyAsync(HttpResponse::body)
                        .join());

        System.out.println("Serial Async took " + String.valueOf(System.currentTimeMillis() - timeStart) + " ms.");
    }
}
